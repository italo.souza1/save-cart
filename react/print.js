import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'

import { formatCartToPrint, formatDate, formatCurrency } from './utils'
import Button from '@vtex/styleguide/lib/Button'
import NewWindow from './newWindow'
import { Button } from 'reactstrap';
import { map } from 'ramda'
const DEFAULT_ADDRESS = {
  city: '',
  complement: '',
  country: '',
  neighborhood: '',
  number: '',
  postalCode: '',
  state: '',
  street: '',
}

class Print extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      isPrinting: false,
    }
    this.handlePrint = this.handlePrint.bind(this)
  }

  handlePrint(popup) {
    return () => {
      this.setState({ printing: true }, () => {
        popup.print()
        this.props.finishedPrinting()
      })
    }
  }

  render() {
    const { cartToPrint, cartLifeSpan, storePreferencesData, storeLogoUrl, finishedPrinting, clientProfileData: { corporateName, corporateDocument: cnpj }, representative } = this.props
    const items = formatCartToPrint(cartToPrint.items, storePreferencesData)
    const creationDate = formatDate(cartToPrint.creationDate)
    const expirationDate = formatDate(cartToPrint.creationDate, cartLifeSpan)
    const { subtotal, discounts, shipping, total, paymentTerm, address } = cartToPrint
    const shippingAddress = address || DEFAULT_ADDRESS
    return (
      <NewWindow onUnload={finishedPrinting}>
        {popup => {
          return (
            <div className="onda-v1">
              <div className="pa4">
                <Row>
                  <div md="10" lg="10"  >
                    <img src={storeLogoUrl} />
                  </div>

                  <div md="2" lg="2" className={`${this.state.printing ? 'dn' : ''}`}>
                    <Button id="vtex-cart-list-print-button" onClick={this.handlePrint(popup)}><FormattedMessage id="list.print" /></Button>
                  </div>
                </Row>
                <Row>
                  <h3 md="12" lg="12">DETALHES DO ORÇAMENTO</h3>
                </Row>

                <Row>
                  <div md="7" lg="7">
                    <h4 classname="mb4">Dados do Cliente</h4>
                    <Row>
                      <div md="3" lg="3">
                        <Text>Cliente:</Text>
                        <Text>CNPJ:</Text>
                        <Text>Endereço:</Text>
                      </div>

                      <div md="9" lg="9">
                        {corporateName} <br> </br>
                        {cnpj} <br> </br>
                        {shippingAddress.street}, {shippingAddress.number} - {shippingAddress.postalCode} - {shippingAddress.neighborhood}, {shippingAddress.city}, {shippingAddress.state}, {shippingAddress.country}
                      </div>
                    </Row>
                  </div>

                  <div md="5" lg="5">
                    <h4 classname="mb4">Dados do Representante</h4>
                    <Row>
                      <div md="3" lg="3">
                        <Text>Nome:</Text>
                        <Text>E-mail:</Text>
                      </div>
                      <div md="9" lg="9">
                        {representative.userName} <br> </br>
                        {representative.userEmail}
                      </div>
                    </Row>
                  </div>
                </Row>

                <Row>
                  <h4 classname="mb4">Dados do Orçamento</h4>
                  <div md={8} lg={8}>
                    <Row>
                      <div md={4} lg={4}>
                        <Text>Nome:</Text>
                        <Text>Prazo de pagamento:</Text>
                        <Text>Tipo de venda:</Text>
                        <Text>Natureza da operação:</Text>
                        <Text>Ordem de compra:</Text>
                        <Text>Periodo de entrega:</Text>
                      </div>
                      <div md={8} lg={8}>
                        {cartToPrint.cartName}<br></br>
                        {paymentTerm} <br></br>
                      </div>
                    </Row>
                  </div>
                  <div md={4} lg={4}>
                    <Row>
                      <div md={5} lg={5}>
                        <Text>Data de criação:</Text>
                        <Text>Data de vencimento:</Text>
                      </div>
                      <div md={7} lg={7}>
                        {creationDate} <br> </br>
                        {expirationDate} <br> </br>

                      </div>
                    </Row>
                  </div>
                </Row>

                {/* <div className="mw5 center ttu f2 pv5 fw6">
                  <FormattedMessage id="quote" />
                </div>
                <div className="mb7">
                  <div className="mb2">
                    <FormattedMessage id="print.representative.name" />: {representative.userName}
                  </div>
                  <div className="mb2">
                    <FormattedMessage id="print.representative.email" />: {representative.userEmail}
                  </div>
                  <div className="mb2">
                    <FormattedMessage id="print.quote.name" />: {cartToPrint.cartName}
                  </div>
                  <div className="mb2">
                    <FormattedMessage id="list.quote.date" />: {creationDate}
                  </div>
                  <div className="mb2">
                    <FormattedMessage id="list.quote.expire" />: {expirationDate}
                  </div>
                  <div className="mb4">
                    <FormattedMessage id="print.paymentterm" />: {paymentTerm}
                  </div>
                  <div className="mb2">
                    <FormattedMessage id="print.address" />:
                  </div>
                  <div className={`${address ? '' : 'dn'}`}>
                  </div>
                </div> */}


                <div className="mt4 mb7">
                  {this.makeTable(items)}
                </div>
                <hr></hr>
                <div>
                  <Row>
                    <div md={{ span: 5, offset: 7 }} lg="">
                      <FormattedMessage id="print.subtotal" />: {formatCurrency(subtotal, storePreferencesData)} <br></br>
                      <FormattedMessage id="print.discounts" />: {formatCurrency(discounts, storePreferencesData)} <br></br>
                      <FormattedMessage id="print.shipping" />: {formatCurrency(shipping, storePreferencesData)} <br></br>
                      <h4> <FormattedMessage id="print.total" />: {formatCurrency(total, storePreferencesData)} </h4> <br></br>
                    </div>
                  </Row>
                </div>
              </div>
            </div>
          )
        }}
      </NewWindow>
    )
  }

  makeTable = (items) => {
    return (
      <div>
        <h4 classname="mb1">Dados do Orçamento</h4>
        <hr></hr>
        <table>
          <tr>
            <th className="fw7 f5 tl pb3"><FormattedMessage id="list.item" /></th>
            <th className="fw7 f5 tl pb3 pl3"><FormattedMessage id="list.quantity" /></th>
            <th className="fw7 f5 tl pb3 pl3"><FormattedMessage id="list.price" /></th>
          </tr>
          {this.makeRows(items)}
        </table>
      </div>
    )
  }

  makeRows = map(item => (
    <tr>
      <td className="fw2 f5 pv6"> <img src={item.imageUrl}></img> {item.name}</td>
      <td className="fw2 f5 pv6 pl3">{item.quantity}</td>
      <td className="fw2 f5 pv6 pl3">{item.formattedPrice}</td>
    </tr>
  ))
}

Print.propTypes = {
  cartToPrint: PropTypes.object,
  cartLifeSpan: PropTypes.number,
  clientProfileData: PropTypes.object,
  finishedPrinting: PropTypes.func,
  storePreferencesData: PropTypes.obj,
  storeLogoUrl: PropTypes.string,
  total: PropTypes.number,
  totalizers: PropTypes.array,
  representative: PropTypes.object,
}

export default Print
